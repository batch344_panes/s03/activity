import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int num = 1;
        int factorialFor = 1;
        int factorialWhile = 1;
        String methodInput;
        ArrayList<String> methodList = new ArrayList<>(Arrays.asList("for", "while"));
        boolean complete = false;
        boolean isFor = true;
        Scanner in = new Scanner(System.in);

        System.out.printf("Choose a loop method to get a factorial: %nfor %nwhile %nChoice: ");
        try{
            while(true){
                methodInput = in.nextLine();
                if(methodList.contains(methodInput.toLowerCase())){
                    break;
                }
                else{
                    System.out.println("Input one of the choices provided.");
                }
            }

            System.out.println("Input an integer whose factorial will be computed: ");

            while (true) {
                num = in.nextInt();
                if (num > 0) {
                    break;
                } else {
                    System.out.println("Please enter a non-negative integer.");
                }
            }

            if(methodInput.equalsIgnoreCase("for")){
                for(int i=2; i<=num; i++){
                    factorialFor *= i;
                }
            }
            else if(methodInput.equalsIgnoreCase("while")){
                int counter=1;
                while(counter<=num){
                    factorialWhile *= counter++;
                }
                isFor = false;
            }
            complete = true;

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            System.out.println((complete) ? ("The factorial of " + num + " is " + ((isFor) ? factorialFor : factorialWhile)) : ("Invalid Input"));
        }

    }
}